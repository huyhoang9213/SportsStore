SportsStore: A Real Application

In the previous chapters, I built quick and simple MVC applications. I described the MVC pattern, the
essential C# features, and the kinds of tools that good MVC developers require. Now it is time to put
everything together and build a simple but realistic e-commerce application.
My application, called SportsStore, will follow the classic approach taken by online stores everywhere.
I will create an online product catalog that customers can browse by category and page, a shopping cart
where users can add and remove products, and a checkout where customers can enter their shipping details.
I will also create an administration area that includes create, read, update, and delete (CRUD) facilities for
managing the catalog, and I will protect it so that only logged-in administrators can make changes.
My goal in this chapter and those that follow is to give you a sense of what real MVC development is like
by creating as realistic an example as possible. I want to focus on the ASP.NET Core MVC, of course, so I have
simplified the integration with external systems, such as the database, and omitted others entirely, such as
payment processing.
You might find the going a little slow as I build up the levels of infrastructure I need, but the initial
investment in an MVC application pays dividends, resulting in maintainable, extensible, well-structured
code with excellent support for unit testing.